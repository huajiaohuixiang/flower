package com.huajiao.review.vo;

import com.huajiao.review.model.FeedReviewEntity;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class FeedReview {
    private long id;
    private long reviewId;
    private long userId;
    private String comment;
    private long likesCount;
    private long commentsCount;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private byte isDeleted;
    private long feedId;

    private String nickName;
    private String avatar;

    public FeedReview(){

    }

    public FeedReview(FeedReviewEntity feedReviewEntity) {
        this.id = feedReviewEntity.getId();
        this.reviewId = feedReviewEntity.getReviewId();
        this.userId = feedReviewEntity.getUserId();
        this.comment = feedReviewEntity.getComment();
        this.likesCount = feedReviewEntity.getLikesCount();
        this.commentsCount = feedReviewEntity.getCommentsCount();
        this.createdAt = feedReviewEntity.getCreatedAt();
        this.updatedAt = feedReviewEntity.getUpdatedAt();
        this.isDeleted = feedReviewEntity.getIsDeleted();
        this.feedId = feedReviewEntity.getFeedId();
    }
    public FeedReview(FeedReviewEntity feedReviewEntity,String nickName, String avatar) {
        this.id = feedReviewEntity.getId();
        this.reviewId = feedReviewEntity.getReviewId();
        this.userId = feedReviewEntity.getUserId();
        this.comment = feedReviewEntity.getComment();
        this.likesCount = feedReviewEntity.getLikesCount();
        this.commentsCount = feedReviewEntity.getCommentsCount();
        this.createdAt = feedReviewEntity.getCreatedAt();
        this.updatedAt = feedReviewEntity.getUpdatedAt();
        this.isDeleted = feedReviewEntity.getIsDeleted();
        this.feedId = feedReviewEntity.getFeedId();
        this.nickName = nickName;
        this.avatar = avatar;
    }
}
