package com.huajiao.user.contract.request;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class AddUserRequest {
    private long userId;
    private String nickname;
    private String password;
    private long stuId;
    private String avatarLink;
}
