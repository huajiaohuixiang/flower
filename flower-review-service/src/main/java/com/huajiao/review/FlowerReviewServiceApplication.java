package com.huajiao.review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

//@EnableSwagger2
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients(basePackages = "com.huajiao.user")
public class FlowerReviewServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FlowerReviewServiceApplication.class, args);
    }

}
