package com.huajiao.review.service;

import com.huajiao.review.model.FeedInfoEntity;
import com.huajiao.review.vo.Feed;
import com.huajiao.response.BaseResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Service
@FeignClient("flower-feed-service")
public interface FeedService {
    //---------------------------查询----------------------------

    @RequestMapping(value = "/feed/getAllFeedInfoEntityPage", method = GET)
    BaseResponse<List<FeedInfoEntity>> findAllFeedInfoEntityPage(@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize);

    @RequestMapping(value = "/feed/getAllFeedPage", method = GET)
    BaseResponse<List<Feed>> findAllFeedPage(@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize);

    @RequestMapping(value = "/feed/getFeedByUserIdPage", method = GET)
    BaseResponse<List<FeedInfoEntity>> findByUserId(@RequestParam("userId") int userId,
                                       @RequestParam("pageNum") int pageNum,
                                       @RequestParam("pageSize") int pageSize);


    @RequestMapping(value = "/feed/getFeedByUserId", method = GET)
    BaseResponse<List<FeedInfoEntity>> findByUserId(@RequestParam("userId") int userId);

    @RequestMapping(value = "/feed/getFeedByFeedId", method = GET)
    BaseResponse<FeedInfoEntity> findByFeedId(@RequestParam("feedId")long feedId);

    @RequestMapping(value = "/feed/existsByFeedId", method = GET)
    BaseResponse<Boolean> existsByFeedId(@RequestParam("feedId")long feedId);

    //---------------------------删除----------------------------
    @RequestMapping(value = "/feed/existsByFeedId", method = POST)
    BaseResponse<FeedInfoEntity> deleteByFeedId(@RequestParam("feedId")long feedId);

    //---------------------------新增----------------------------
    //---------------------------更改----------------------------
    @RequestMapping(value = "/feed/save", method = POST)
    BaseResponse<FeedInfoEntity> save(FeedInfoEntity feedInfoEntity);

}
