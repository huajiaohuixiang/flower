package com.huajiao.review.vo;

import lombok.Data;


@Data
public class CreateReview {
    private long userId;
    private String comment;
    private long feedId;
}
