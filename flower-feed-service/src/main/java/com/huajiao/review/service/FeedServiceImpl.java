package com.huajiao.review.service;


import com.huajiao.review.domain.FeedDomain;
import com.huajiao.review.model.FeedInfoEntity;
import com.huajiao.review.vo.Feed;
import com.huajiao.response.BaseResponse;
import com.huajiao.response.BaseResponseUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;


@RestController
public class FeedServiceImpl implements FeedService {

    @Autowired
    FeedDomain feedDomain;


    //---------------------------查询----------------------------
    @Override
    @RequestMapping(value = "/feed/getAllFeedPage", method = GET)
    public BaseResponse<List<Feed>> findAllFeedPage(@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize){
        return BaseResponseUtil.ofSuccess(feedDomain.findAllFeed(pageNum,pageSize));
    }
    @Override
    @RequestMapping(value = "/feed/getAllFeedInfoEntityPage", method = GET)
    public BaseResponse<List<FeedInfoEntity>> findAllFeedInfoEntityPage(@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize){
        return BaseResponseUtil.ofSuccess(feedDomain.findAll(pageNum,pageSize));
    }

    @Override
    @RequestMapping(value = "/feed/getFeedByUserIdPage", method = GET)
    public BaseResponse<List<FeedInfoEntity>> findByUserId(@RequestParam("userId") int userId,
                                                    @RequestParam("pageNum") int pageNum,
                                                    @RequestParam("pageSize") int pageSize){
        return  BaseResponseUtil.ofSuccess(feedDomain.findByUserId(userId,pageNum,pageSize));
    }

    @Override
    @RequestMapping(value = "/feed/getFeedByUserId", method = GET)
    public BaseResponse<List<FeedInfoEntity>> findByUserId(@RequestParam("userId") int userId) {
        return BaseResponseUtil.ofSuccess(feedDomain.findByUserId(userId));
    }

    @Override
    @RequestMapping(value = "/feed/getFeedByFeedId", method = GET)
    public BaseResponse<FeedInfoEntity> findByFeedId(@RequestParam("feedId")long feedId) {
        return BaseResponseUtil.ofSuccess(feedDomain.findByFeedId(feedId));
    }

    @Override
    @RequestMapping(value = "/feed/existsByFeedId", method = GET)
    public BaseResponse<Boolean> existsByFeedId(@RequestParam("feedId")long feedId) {
        return BaseResponseUtil.ofSuccess(feedDomain.existsByFeedId(feedId));
    }



    //---------------------------删除----------------------------
    @Override
    @RequestMapping(value = "/feed/existsByFeedId", method = POST)
    public BaseResponse<FeedInfoEntity> deleteByFeedId(@RequestParam("feedId")long feedId) {
        FeedInfoEntity feedInfoEntity = feedDomain.findByFeedId(feedId);
        feedInfoEntity.setIsDeleted((byte) 1);
        return BaseResponseUtil.ofSuccess(feedDomain.save(feedInfoEntity));
    }




    //---------------------------新增----------------------------
    //---------------------------更改----------------------------
    @Override
    @RequestMapping(value = "/feed/save", method = POST)
    public BaseResponse<FeedInfoEntity> save(FeedInfoEntity feedInfoEntity) {
        return BaseResponseUtil.ofSuccess(feedDomain.save(feedInfoEntity));
    }

}
