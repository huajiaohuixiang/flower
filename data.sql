create table feed_info
(
    id bigint auto_increment comment '系统自增主键
',
    feed_id bigint not null comment '业务id',
    content varchar(4096) not null comment '动态内容',
    user_id bigint not null comment '用户id',
    likes_count bigint default 0 not null comment '点赞数',
    comments_count bigint default 0 not null,
    views_count bigint default 0 not null,
    created_at datetime not null,
    updated_at datetime not null,
    is_deleted tinyint(1) default 0 not null,
    photo_links json null,
    constraint feed_info_feed_id_uindex
        unique (feed_id),
    constraint feed_info_id_uindex
        unique (id)
);

create index feed_info_feed_id_index
    on feed_info (feed_id);

create index feed_info_user_id_is_deleted_index
    on feed_info (user_id, is_deleted);

alter table feed_info
    add primary key (id);

create table feed_review
(
    id bigint auto_increment,
    review_id bigint not null,
    user_id bigint not null,
    comment varchar(2048) not null,
    likes_count bigint default 0 not null,
    comments_count bigint default 0 not null,
    created_at datetime not null,
    updated_at datetime not null,
    is_deleted tinyint(1) default 0 not null,
    feed_id bigint not null,
    constraint feed_review_id_uindex
        unique (id),
    constraint feed_review_review_id_uindex
        unique (review_id)
);

alter table feed_review
    add primary key (id);

create table user_info
(
    id bigint auto_increment comment '系统自动自增主键',
    user_id bigint not null comment '业务上的用户id，随机算法生成',
    nickname varchar(100) not null comment '用户昵称',
    password varchar(20) not null comment '密码',
    stu_id bigint not null comment '学号',
    created_at datetime not null,
    updated_at datetime not null,
    is_deleted tinyint(1) not null,
    avatar_link varchar(2048) default '/avatar/default.png' not null,
    constraint user_info_id_uindex
        unique (id),
    constraint user_info_nickname_uindex
        unique (nickname),
    constraint user_info_user_id_uindex
        unique (user_id)
);

create index user_info_user_id_index
    on user_info (user_id);

alter table user_info
    add primary key (id);

