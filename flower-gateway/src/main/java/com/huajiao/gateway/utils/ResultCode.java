package com.huajiao.gateway.utils;

public interface ResultCode {

    public static int SUCCESS = 20000;

    public static int ERROR = 20001;
}
