package com.huajiao.review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class FlowerFeedContractApplication {

    public static void main(String[] args) {
        SpringApplication.run(FlowerFeedContractApplication.class, args);
    }

}
