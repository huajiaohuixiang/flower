package com.huajiao.review.vo;


import com.huajiao.review.model.FeedInfoEntity;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Data
public class Feed implements Serializable {

    private long id;
    private long feedId;
    private String content;
    private long userId;
    private long likesCount;
    private long commentsCount;
    private long viewsCount;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private byte isDeleted;

    private List<String> photoLinks;

    private String nickname;

    private String avatar;


    // 当前用户是否点赞
    private boolean like;

    // 点赞列表
    private List<String> likesList;

    public Feed(FeedInfoEntity feedInfoEntity){
        this.id = feedInfoEntity.getId();
        this.feedId = feedInfoEntity.getFeedId();
        this.content = feedInfoEntity.getContent();
        this.userId = feedInfoEntity.getUserId();
        this.likesCount = feedInfoEntity.getLikesCount();
        this.commentsCount = feedInfoEntity.getCommentsCount();
        this.viewsCount = feedInfoEntity.getCommentsCount();
        this.createdAt = feedInfoEntity.getCreatedAt();
        this.updatedAt = feedInfoEntity.getUpdatedAt();
        this.isDeleted = feedInfoEntity.getIsDeleted();
        this.photoLinks = (List<String>) feedInfoEntity.getPhotoLinks();
    }

}
