package com.huajiao.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BaseResponse<T> {
    private int errorCode;
    private String errorMsg;
    private Boolean result;
    private T data;
}
