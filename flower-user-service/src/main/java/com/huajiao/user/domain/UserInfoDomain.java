package com.huajiao.user.domain;


import com.huajiao.user.contract.model.UserInfoEntity;
import com.huajiao.user.contract.request.AddUserRequest;
import com.huajiao.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class UserInfoDomain {
    @Autowired
    UserRepository userRepository;


    public UserInfoEntity getUserByUserId( Long userId) {
        return userRepository.findByUserId(userId);
    }


    public UserInfoEntity addUser(UserInfoEntity userInfoEntity) {
        Timestamp timestamp = new java.sql.Timestamp(new java.util.Date().getTime());
        userInfoEntity.setCreatedAt(timestamp);
        userInfoEntity.setUpdatedAt(timestamp);
        //TODO 加密
        userInfoEntity.setPassword(userInfoEntity.getPassword());
        return userRepository.save(userInfoEntity);
    }



    public UserInfoEntity deleteUser(Long userId) {
        UserInfoEntity userInfoEntity = userRepository.findByUserId(userId);
        userInfoEntity.setIsDeleted((byte) 1);
        userRepository.save(userInfoEntity);
        return userInfoEntity;
    }


    public UserInfoEntity updateUser(UserInfoEntity userInfoEntity) {
        return userRepository.save(userInfoEntity);
    }

}
