package com.huajiao.user.repository;

import com.huajiao.user.contract.model.UserInfoEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<UserInfoEntity,Long> {
    //---------------------------查询----------------------------
    List<UserInfoEntity> findByNickname(String nickName);

    UserInfoEntity findByUserId(long userId);

    boolean existsByUserId(long userId);

    //---------------------------删除----------------------------
    void deleteByUserId(long userId);

    //---------------------------新增----------------------------
    UserInfoEntity save(UserInfoEntity userInfoEntity);


    //---------------------------更改----------------------------

}
