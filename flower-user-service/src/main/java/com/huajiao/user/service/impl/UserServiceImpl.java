package com.huajiao.user.service.impl;

import com.huajiao.user.contract.model.UserInfoEntity;
import com.huajiao.user.contract.service.UserService;
import com.huajiao.user.domain.UserInfoDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class UserServiceImpl implements UserService {
    @Autowired
    UserInfoDomain userInfoDomain;

    @RequestMapping(value = "/user/getUserByUserId", method = GET)
    @Override
    public UserInfoEntity getUserByUserId(@RequestParam(value = "userId") Long userId) {
        System.out.println("getUser:" + userId);
        return userInfoDomain.getUserByUserId(userId);
    }

    @Override
    public UserInfoEntity addUser(UserInfoEntity userInfoEntity) {
        return userInfoDomain.addUser(userInfoEntity);
    }

    @Override
    @RequestMapping(value = "/user/getUserByUserId", method = POST)
    public UserInfoEntity deleteUser(Long userId) {
        return userInfoDomain.deleteUser(userId);
    }

    @Override
    public UserInfoEntity updateUser(UserInfoEntity userInfoEntity) {
        return userInfoDomain.updateUser(userInfoEntity);
    }
}
