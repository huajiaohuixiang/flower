package com.huajiao.review.model;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@TypeDef(name = "json", typeClass = JsonStringType.class)
@Table(name = "feed_info", schema = "flower")
public class FeedInfoEntity {
    private long id;
    private long feedId;
    private String content;
    private long userId;
    private long likesCount;
    private long commentsCount;
    private long viewsCount;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private byte isDeleted;

    private Object photoLinks;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "feed_id")
    public long getFeedId() {
        return feedId;
    }

    public void setFeedId(long feedId) {
        this.feedId = feedId;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "likes_count")
    public long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(long likesCount) {
        this.likesCount = likesCount;
    }

    @Basic
    @Column(name = "comments_count")
    public long getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(long commentsCount) {
        this.commentsCount = commentsCount;
    }

    @Basic
    @Column(name = "views_count")
    public long getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(long viewsCount) {
        this.viewsCount = viewsCount;
    }

    @Basic
    @Column(name = "created_at")
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "updated_at")
    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Basic
    @Column(name = "is_deleted")
    public byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Basic
    @Type(type = "json")
    @Column(name = "photo_links", columnDefinition = "json")
    public Object getPhotoLinks() {
        return photoLinks;
    }

    public void setPhotoLinks(Object photoLinks) {
        this.photoLinks = photoLinks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FeedInfoEntity that = (FeedInfoEntity) o;
        return id == that.id && feedId == that.feedId && userId == that.userId && likesCount == that.likesCount && commentsCount == that.commentsCount && viewsCount == that.viewsCount && isDeleted == that.isDeleted && Objects.equals(content, that.content) && Objects.equals(createdAt, that.createdAt) && Objects.equals(updatedAt, that.updatedAt) && Objects.equals(photoLinks, that.photoLinks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, feedId, content, userId, likesCount, commentsCount, viewsCount, createdAt, updatedAt, isDeleted, photoLinks);
    }
}
