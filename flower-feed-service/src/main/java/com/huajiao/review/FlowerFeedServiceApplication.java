package com.huajiao.review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = "com.huajiao.user.contract.service")
@EnableDiscoveryClient
@SpringBootApplication
//@EntityScan("com.huajiao.feed.model")
//@EnableJpaRepositories("com.huajiao.feed.repository")
public class FlowerFeedServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(FlowerFeedServiceApplication.class, args);
    }

}
