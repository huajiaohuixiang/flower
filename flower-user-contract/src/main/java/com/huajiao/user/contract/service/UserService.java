package com.huajiao.user.contract.service;

import com.huajiao.user.contract.model.UserInfoEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;


@Service
@FeignClient("flower-user-service")
public interface UserService {
    @RequestMapping(value = "/user/getUserByUserId", method = GET)
    UserInfoEntity getUserByUserId(@RequestParam(value = "userId") Long userId);

    @RequestMapping(value = "/user/addUser", method = POST)
    UserInfoEntity addUser(UserInfoEntity userInfoEntity);

    @RequestMapping(value = "/user/deleteUser", method = POST)
    UserInfoEntity deleteUser(Long userId);

    @RequestMapping(value = "/user/updateUser", method = POST)
    UserInfoEntity updateUser(UserInfoEntity userInfoEntity);

}
