package com.huajiao.review.service.iml;


import com.huajiao.response.BaseResponse;
import com.huajiao.response.BaseResponseUtil;
import com.huajiao.review.domain.FeedReviewDomain;
import com.huajiao.review.model.FeedReviewEntity;
import com.huajiao.review.service.FeedReviewService;
import com.huajiao.review.vo.CreateReview;
import com.huajiao.review.vo.FeedReview;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/review")
public class FeedReviewServiceImpl implements FeedReviewService {

    @Resource
    FeedReviewDomain feedReviewDomain;


    //---------------------------查询----------------------------
    @GetMapping("/findByReviewId")
    public BaseResponse<FeedReview> findByReviewId(@RequestParam("reviewId") Long reviewId) {
        return BaseResponseUtil.ofSuccess(feedReviewDomain.findByReviewId(reviewId));
    }

    @GetMapping("/findByUserIdPage")
    public BaseResponse<List<FeedReview>> findByUserIdPage(
            @RequestParam("userId")long userId,
            @RequestParam("pageNum")int pageNum,
            @RequestParam("pageSize")int pageSize) {
        return BaseResponseUtil.ofSuccess(feedReviewDomain.findByUserIdPage(userId,pageNum,pageSize));
    }

    @GetMapping("/findByUserId")
    public BaseResponse<List<FeedReview>> findByUserId(@RequestParam("userId")long userId) {
        return BaseResponseUtil.ofSuccess(feedReviewDomain.findByUserId(userId));
    }

    @GetMapping("/findByFeedIdPage")
    public BaseResponse<List<FeedReview>> findByFeedIdPage(
            @RequestParam("feedId")long feedId,
            @RequestParam("pageNum")int pageNum,
            @RequestParam("pageSize")int pageSize) {
        return BaseResponseUtil.ofSuccess(feedReviewDomain.findByFeedIdPage(feedId,pageNum,pageSize));
    }


    @GetMapping("/findByFeedId")
    public BaseResponse<List<FeedReview>> findByFeedId(@RequestParam("feedId")long feedId) {
        return BaseResponseUtil.ofSuccess(feedReviewDomain.findByFeedId(feedId));
    }


    @GetMapping("/existsByReviewId")
    public BaseResponse<Boolean> existsByReviewId(@RequestParam("reviewId")long reviewId) {
        return BaseResponseUtil.ofSuccess(feedReviewDomain.existsByReviewId(reviewId));
    }

    //---------------------------删除----------------------------
    @PostMapping("/deleteByReviewId")
    public BaseResponse<Boolean> deleteByReviewId(@RequestParam("reviewId")long reviewId) {
        feedReviewDomain.deleteByReviewId(reviewId);
        return BaseResponseUtil.ofSuccess(true);
    }


    //---------------------------更改----------------------------
    //---------------------------新增----------------------------
    @PostMapping("/save")
    public BaseResponse<FeedReviewEntity> save(FeedReviewEntity feedReviewEntity) {
        return BaseResponseUtil.ofSuccess(feedReviewDomain.save(feedReviewEntity));
    }

    @PostMapping("/sendReview")
    public BaseResponse<FeedReviewEntity> sendReview(@RequestBody CreateReview feedReviewEntity) {
        return BaseResponseUtil.ofSuccess(feedReviewDomain.sendReview(feedReviewEntity));
    }




}
