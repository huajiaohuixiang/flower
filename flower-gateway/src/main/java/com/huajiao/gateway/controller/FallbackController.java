package com.huajiao.gateway.controller;

import com.huajiao.gateway.utils.Response;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class FallbackController {

    @RequestMapping("/fallbackA")
    public Response fallbackA(){
        Response response = new Response();
        response.setCode(100);
        response.setMessage("服务暂时不可用");
        return response;
    }
}
