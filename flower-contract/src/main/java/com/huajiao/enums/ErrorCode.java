package com.huajiao.enums;

import lombok.Data;

public enum ErrorCode {
    SYSTEM_EXCEPTION(10001,"系统异常");


    int code;
    String msg;

    ErrorCode(int i, String 系统异常) {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
