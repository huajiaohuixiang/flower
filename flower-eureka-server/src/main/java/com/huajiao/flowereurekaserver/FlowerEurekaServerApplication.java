package com.huajiao.flowereurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


@EnableEurekaServer
@SpringBootApplication
public class FlowerEurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FlowerEurekaServerApplication.class, args);
    }

}
