package com.huajiao.review.domain;

import com.huajiao.review.model.FeedReviewEntity;
import com.huajiao.review.repository.FeedReviewRepository;
import com.huajiao.review.vo.CreateReview;
import com.huajiao.review.vo.FeedReview;
import com.huajiao.user.contract.model.UserInfoEntity;
import com.huajiao.user.contract.service.UserService;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FeedReviewDomain {

    @Resource
    FeedReviewRepository feedReviewRepository;

    @Resource
    UserService userService;

    //---------------------------查询----------------------------
    public FeedReview findByReviewId(Long reviewId) {
        FeedReviewEntity feedReviewEntity = feedReviewRepository.findByReviewId(reviewId);
        UserInfoEntity userInfoEntity = userService.getUserByUserId(feedReviewEntity.getUserId());
        return new FeedReview(feedReviewEntity,userInfoEntity.getNickname(),userInfoEntity.getAvatarLink());
    }



    public List<FeedReview> findByUserId(long userId) {
        List<FeedReviewEntity> feedReviewEntities = feedReviewRepository.findByUserId(userId);
        List<FeedReview> feedReviews = feedReviewEntities.stream().map(feedReviewEntity -> {
            UserInfoEntity userInfoEntity = userService.getUserByUserId(feedReviewEntity.getUserId());
            return new FeedReview(feedReviewEntity,userInfoEntity.getNickname(),userInfoEntity.getAvatarLink());
        }).collect(Collectors.toList());

        return feedReviews;
    }

    public List<FeedReview> findByFeedId(long feedId) {
        List<FeedReviewEntity> feedReviewEntities = feedReviewRepository.findByFeedId(feedId);
        List<FeedReview> feedReviews = feedReviewEntities.stream().map(feedReviewEntity -> {
            UserInfoEntity userInfoEntity = userService.getUserByUserId(feedReviewEntity.getUserId());
            return new FeedReview(feedReviewEntity,userInfoEntity.getNickname(),userInfoEntity.getAvatarLink());
        }).collect(Collectors.toList());

        return feedReviews;
    }
    public List<FeedReview> findByUserIdPage(long userId,int pageNum,int pageSize) {
        List<FeedReviewEntity> feedReviewEntities = feedReviewRepository.findByUserId(userId,PageRequest.of(pageNum,pageSize)).getContent();
        List<FeedReview> feedReviews = feedReviewEntities.stream().map(feedReviewEntity -> {
            UserInfoEntity userInfoEntity = userService.getUserByUserId(feedReviewEntity.getUserId());
            return new FeedReview(feedReviewEntity,userInfoEntity.getNickname(),userInfoEntity.getAvatarLink());
        }).collect(Collectors.toList());

        return feedReviews;

    }

    public List<FeedReview> findByFeedIdPage(long feedId,int pageNum,int pageSize) {
        List<FeedReviewEntity> feedReviewEntities = feedReviewRepository.findByFeedId(feedId,PageRequest.of(pageNum,pageSize)).getContent();
        List<FeedReview> feedReviews = feedReviewEntities.stream().map(feedReviewEntity -> {
            UserInfoEntity userInfoEntity = userService.getUserByUserId(feedReviewEntity.getUserId());
            return new FeedReview(feedReviewEntity,userInfoEntity.getNickname(),userInfoEntity.getAvatarLink());
        }).collect(Collectors.toList());

        return feedReviews;

    }

    public boolean existsByReviewId(long reviewId) {
        return feedReviewRepository.existsByReviewId(reviewId);
    }

    //---------------------------删除----------------------------
    public void deleteByReviewId(long reviewId) {
        feedReviewRepository.deleteByReviewId(reviewId);
    }


    //---------------------------更改----------------------------
    //---------------------------新增----------------------------
    public FeedReviewEntity save(FeedReviewEntity feedReviewEntity) {
        return feedReviewRepository.save(feedReviewEntity);
    }

    public FeedReviewEntity sendReview(CreateReview createReview) {
        FeedReviewEntity feedReviewEntity = new FeedReviewEntity();
        feedReviewEntity.setReviewId(1L);
        feedReviewEntity.setFeedId(createReview.getFeedId());
        feedReviewEntity.setUserId(createReview.getUserId());
        feedReviewEntity.setComment(createReview.getComment());
        Timestamp timestamp = new java.sql.Timestamp(new java.util.Date().getTime());
        feedReviewEntity.setCreatedAt(timestamp);
        feedReviewEntity.setUpdatedAt(timestamp);
        return feedReviewRepository.save(feedReviewEntity);
    }



}
