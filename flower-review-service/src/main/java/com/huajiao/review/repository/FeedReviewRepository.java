package com.huajiao.review.repository;

import com.huajiao.review.model.FeedReviewEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface FeedReviewRepository extends CrudRepository<FeedReviewEntity,Long> {
    //---------------------------查询----------------------------
    FeedReviewEntity findByReviewId(Long reviewId);

    List<FeedReviewEntity> findByUserId(long userId);

    List<FeedReviewEntity> findByFeedId(long feedId);

    Page<FeedReviewEntity> findByUserId(long userId, Pageable pageable);

    Page<FeedReviewEntity> findByFeedId(long feedId,Pageable pageable);
    boolean existsByReviewId(long reviewId);

    //---------------------------删除----------------------------
    void deleteByReviewId(long reviewId);


    //---------------------------更改----------------------------
    //---------------------------新增----------------------------
    FeedReviewEntity save(FeedReviewEntity feedReviewEntity);



}
