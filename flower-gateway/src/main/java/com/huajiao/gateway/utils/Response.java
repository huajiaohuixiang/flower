package com.huajiao.gateway.utils;

import java.util.HashMap;
import java.util.Map;

public class Response {
    private boolean success;

    private int code;

    private String message;

    private Map<String, Object> data = new HashMap<String, Object>();

    public Response() {
    }

    public static Response ok() {
        Response r = new Response();
        r.setSuccess(true);
        r.setCode(ResultCode.SUCCESS);
        r.setMessage("成功");
        return r;
    }

    public static Response error() {
        Response r = new Response();
        r.setSuccess(false);
        r.setCode(ResultCode.ERROR);
        r.setMessage("失败");
        return r;
    }

    public Response success(Boolean success) {
        this.setSuccess(success);
        return this;
    }

    public Response message(String message) {
        this.setMessage(message);
        return this;
    }

    public Response code(int code) {
        this.setCode(code);
        return this;
    }

    public Response data(String key, Object value) {
        this.data.put(key, value);
        return this;
    }

    public Response data(Map<String, Object> map) {
        this.setData(map);
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
