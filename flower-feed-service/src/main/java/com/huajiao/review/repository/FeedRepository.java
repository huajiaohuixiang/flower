package com.huajiao.review.repository;

import com.huajiao.review.model.FeedInfoEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FeedRepository extends CrudRepository<FeedInfoEntity,Long> {
    //---------------------------查询----------------------------
    Page<FeedInfoEntity> findAll(Pageable pageable);

    Page<FeedInfoEntity> findByUserId(long userId, Pageable pageable);

    List<FeedInfoEntity> findByUserId(long userId);

    FeedInfoEntity findByFeedId(long feedId);

    boolean existsByFeedId(long feedId);

    //---------------------------删除----------------------------
    void deleteByFeedId(long feedId);

    //---------------------------新增----------------------------
    //---------------------------更改----------------------------
    FeedInfoEntity save(FeedInfoEntity feedInfoEntity);

}
