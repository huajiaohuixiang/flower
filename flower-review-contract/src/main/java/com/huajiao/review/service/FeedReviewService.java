package com.huajiao.review.service;


import com.huajiao.response.BaseResponse;
import com.huajiao.review.model.FeedReviewEntity;
import com.huajiao.review.vo.CreateReview;
import com.huajiao.review.vo.FeedReview;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
@FeignClient("flower-review-service")
public interface FeedReviewService {

    //---------------------------查询----------------------------
    @GetMapping("/findByReviewId")
    BaseResponse<FeedReview> findByReviewId(@RequestParam("reviewId") Long reviewId) ;

    @GetMapping("/findByUserId")
    BaseResponse<List<FeedReview>> findByUserId(@RequestParam("userId")long userId) ;
    @GetMapping("/findByUserIdPage")
    BaseResponse<List<FeedReview>> findByUserIdPage(
            @RequestParam("userId")long userId,
            @RequestParam("pageNum")int pageNum,
            @RequestParam("pageSize")int pageSize) ;

    @GetMapping("/findByFeedId")
    BaseResponse<List<FeedReview>> findByFeedId(@RequestParam("feedId")long feedId) ;

    @GetMapping("/findByFeedIdPage")
    BaseResponse<List<FeedReview>> findByFeedIdPage(
            @RequestParam("feedId")long feedId,
            @RequestParam("pageNum")int pageNum,
            @RequestParam("pageSize")int pageSize) ;

    @GetMapping("/existsByReviewId")
    BaseResponse<Boolean> existsByReviewId(@RequestParam("reviewId")long reviewId) ;

    //---------------------------删除----------------------------
    @PostMapping("/deleteByReviewId")
    BaseResponse<Boolean> deleteByReviewId(@RequestParam("reviewId")long reviewId) ;


    //---------------------------更改----------------------------
    //---------------------------新增----------------------------
    @PostMapping("/save")
    BaseResponse<FeedReviewEntity> save(FeedReviewEntity feedReviewEntity) ;

    @PostMapping("/sendReview")
    public BaseResponse<FeedReviewEntity> sendReview(CreateReview feedReviewEntity) ;
}
