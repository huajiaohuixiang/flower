package com.huajiao.response;


public class BaseResponseUtil {
    public static <T> BaseResponse<T> ofSuccess(T data){
        return BaseResponse.<T>builder()
                .errorCode(0)
                .errorMsg("success")
                .result(true)
                .data(data)
                .build();
    }

    public static <T> BaseResponse<T> ofFail(int errorCode, String errorMsg){
        return BaseResponse.<T>builder()
                .errorCode(errorCode)
                .errorMsg(errorMsg)
                .result(false)
                .build();
    }
}
