package com.huajiao.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SystemException extends Exception{
    private int code;
}
